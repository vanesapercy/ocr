'''
Created on Nov 4, 2019

@author: vadmin
'''

#Importando librerias
import io
from PIL import Image
import pytesseract
from wand.image import Image as wi

#Abrir el archivo PDF
pdf = wi(filename = "/home/vadmin/Downloads/present continuos.pdf", resolution = 300)
#Convertir el archivo PDF a jpeg
pdfImage = pdf.convert('jpeg')

#Lista para guardar el PDF convertido a jpeg
imageBlobs = []

#Recorrer los blob de imagenes y agregarlos a la lista imageBlobs 
for img in pdfImage.sequence:
    imgPage = wi(image = img)
    imageBlobs.append(imgPage.make_blob('jpeg'))

#Lista para guardar  el texto reconocido
recognized_text = []

#Ejecutar OCR sobre los blob de imagenes
for imgBlob in imageBlobs:
    im = Image.open(io.BytesIO(imgBlob))
    text = pytesseract.image_to_string(im, lang = 'eng')
    #Agregar el texto reconocido a la lista recognized_text
    recognized_text.append(text)

#Imprimir texto reconocido 
print(recognized_text[1])

