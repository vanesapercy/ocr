'''
Created on Nov 4, 2019

@author: vadmin
'''
#Importando librerias 
from PIL import Image
import pytesseract

#Abriendo la imagen con el metodo open de la clase Image
im = Image.open("/home/vadmin/Pictures/prueba.png")

#Pasando la imagen al metodo image_to_string de la clase pytesseract
text = pytesseract.image_to_string(im, lang = 'eng')

#Mostrando el resultado en consola
print(text)
